/*
 *  Ng_Melissa_a1.c
 *  melng
 *
 *  Created by AppleMeiMei on 8/25/09.
 *  Copyright 2009 __MyCompanyName__. All rights reserved.
 *
 */

#include <stdlib.h>
#include <GL/glut.h>
#include <math.h>
#define N 10
#define PI 3.14159

static double x_offset = -50;
static double x_speed = 1;
static double y_offset;
static double angle, new_y_offset;
static int pause = 0;
static int shape = 1;

void init(void)
{
	 // setup opengl state
	 glClearColor(0,0,0,1.0);
	 glColor3f(1,1,1);
}


// Setup the viewing transform
// This setup up a display with coordiantes x axis -50 to 50, y axis -50 to 50
// This callback is called anytime the window size changes (for example, you resize)
// in Windows
void reshape(int w, int h)
{
   glViewport(0, 0, w, h);       // maps to window 0,0, with * height
   glMatrixMode(GL_PROJECTION);  // set the projection matrix (will talk about this later)
   glLoadIdentity();             // This clear any existing matrix

    // This is a common approach in OpenGL  -- the world does is always the same, no matter
    // the window dim.  So, we need to normalize in case the width and height
   if (w <= h)                    // normalize by which dimensin is longer
      gluOrtho2D (-50.0, 50.0,
         -50.0*(GLfloat)h/(GLfloat)w, 50.0*(GLfloat)h/(GLfloat)w);
   else
      gluOrtho2D (-50.0*(GLfloat)w/(GLfloat)h,
         50.0*(GLfloat)w/(GLfloat)h, -50.0, 50.0);

   glMatrixMode(GL_MODELVIEW);    // set model transformation
   glLoadIdentity();              // to be empty (will talk about this later
}

// Draw a square about the origin
void drawBox(double x_offset, double y_offset)
{
    switch (shape)
	{
	case 1:
	// yellow ninja
	glBegin(GL_POLYGON);
	glColor3f(1,0,1);
    glVertex2f(x_offset, 1.0+y_offset);
    glVertex2f(0.3+x_offset, 0.3+y_offset);
    glVertex2f(1.0+x_offset, y_offset);
    glVertex2f(0.3+x_offset, -0.3+y_offset);
    glVertex2f(x_offset, -1.0+y_offset);
    glVertex2f(-0.3+x_offset, -0.3+y_offset);
    glVertex2f(-1.0+x_offset, y_offset);
    glVertex2f(-0.3+x_offset, 0.3+y_offset);

    glEnd();

	glFlush();
	break;

	case 2:
	//red heart
    glBegin(GL_POLYGON);
	glColor3f(1,0,0);
    glVertex2f(x_offset, 0.5+y_offset);    // v1    v4 --> v1
    glVertex2f(0.5+x_offset, 0.75+y_offset);   // v2    ^      |
    glVertex2f(1.0+x_offset,y_offset);   // v3    |      v
    glVertex2f(x_offset, -1.0+y_offset);   // v4   v3 ---  v2
    glVertex2f(-1.0+x_offset, y_offset);    // v1    v4 --> v1
    glVertex2f(-0.5+x_offset, 0.75+y_offset);   // v2    ^      |

    glEnd();

    glFlush();
    break;

    case 3:
    // blue cross
    glBegin(GL_POLYGON);
	glColor3f(0,1,1);
    glVertex2f(0.25+x_offset, 0.75+y_offset);    // v1    v4 --> v1
    glVertex2f(0.25+x_offset, 0.25+y_offset);   // v2    ^      |
    glVertex2f(1.0+x_offset,y_offset);   // v3    |      v
    glVertex2f(0.25+x_offset, -0.25+y_offset);   // v4   v3 ---  v2
    glVertex2f(0.25+x_offset, -0.75+y_offset);    // v1    v4 --> v1
    glVertex2f(-0.25+x_offset, -0.75+y_offset);   // v2    ^      |
    glVertex2f(-0.25+x_offset, -0.25+y_offset);    // v1    v4 --> v1
    glVertex2f(-0.75+x_offset, -0.25+y_offset);   // v3    |      v
    glVertex2f(-0.75+x_offset, 0.25+y_offset);   // v2    ^      |
    glVertex2f(-0.25+x_offset, 0.25+y_offset);    // v1    v4 --> v1
    glVertex2f(-0.25+x_offset, 0.75+y_offset);   // v3    |      v

    glEnd();

    glFlush();
    }
}

/* Display Function */
void display(void)
{
    static int boxArray[100];

	 // Clear Color = black
	 // Clear the buffer
    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT);

	int j;
	for(j=0;j<N;j++)
	{

        drawBox(x_offset, y_offset);     // Draw with offset
    }

	glClearColor(0,0,0,0);
	glClear(GL_COLOR_BUFFER_BIT);

	angle = (double)((rand()%3001)/100.00) - 15; // to generate angles with 2 dp from -15 to 15
    new_y_offset = (double) tan ((double)(angle * PI / 180));  //no. of units moving along the y axis

	y_offset += new_y_offset;        //  Advance offset
	x_offset += x_speed;           // x_offset will move 5 units at a time

	drawBox(x_offset, y_offset);

	if (x_offset >= 50) //out of the boundary
	{
        x_offset = -50; // move back to the left side of the screen
		y_offset = 0;// return back to original y_offset
	}

	 glutSwapBuffers();   // Using double-buffer, swap the buffer to display
}

// SETUP KEYBOARD -- this program updates the square when the user presses ' '(space))
void keyboard(unsigned char key, int x, int y)
{
   switch (key) {

		case ' ':    /* Call display function */
         	if(pause == 0)
			pause = 1;
			else pause = 0;
         break;

		case '1':
			shape = 1; // ninja star
			break;

		case '2':
			shape = 2; // heart
			break;

		case '3':
		shape = 3; // cross
			break;

		case 's' :
		  if(x_speed >0.5)
		  x_speed *= 0.5;
		  else
		  x_speed = 0.5;
		  break;

		case 'f':
		  if(x_speed < 10)
		  x_speed *= 2;
		  else
		  x_speed = 10;
		  break;

		case 27:  /*  Escape Key  */
         exit(0);
         break;

      default:
	     if(pause==0) glutPostRedisplay();
         break;
    }
}

// Idle call back
void idle()
{
	if(pause==0) glutPostRedisplay();
}

/*  Main Loop
 *  Open window with initial window size, title bar,
 *  RGB display mode, and handle input events.
 *
 *  We have registered a keyboard function.  The animation of the
 *  square is updated each time you press ' '.
 */
int main(int argc, char** argv)
{
   glutInit(&argc, argv);   // not necessary unless on Unix
   glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
   glutInitWindowSize (512, 512);
   glutCreateWindow ("Melissa_ng_a1");
   init();

   glutReshapeFunc (reshape);       // register respace (anytime window changes)
   glutKeyboardFunc (keyboard);     // register keyboard (anytime keypressed)
   glutDisplayFunc (display);       // register display function
   glutIdleFunc (idle);             // reister idle function
   x_speed = rand()% 5;
   glutMainLoop();

   return 0;
}
