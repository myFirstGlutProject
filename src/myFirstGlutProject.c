/*
 ============================================================================
 Name        : myFirstGlutProject.c
 Author      : melynx
 Version     : 0.01
 Copyright   :
 Description : Flying boxes
 ============================================================================
 */

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#include <GL/gl.h>
#define MAX_N 10

/* global variables */
int lastFrameTime = 0;
int animateFlag = 0;
int yAxis[MAX_N+1];
int ranAngle[MAX_N];
int N;
int currentBox = -1;

float boxX = 0.0f;
float boxY = 0.0f;
float speed = 200;

void drawBox(int x_offset,int y_offset)
{
    glBegin(GL_QUADS);
    glVertex2f(x_offset, y_offset);
    glVertex2f(x_offset+20.0f, y_offset);
    glVertex2f(x_offset+20.0f, y_offset+20.0f);
    glVertex2f(x_offset, y_offset+20.0f);
    glEnd();
}

void display(void)
{
	int x;	//counter
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // draw stationary boxes
    for (x=0;x<N;x++)
    {
    		drawBox(0,yAxis[x]);
    }

    glutSwapBuffers();
}

void animation(void)
{
	int x;	//counter
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (lastFrameTime == 0)
    {
        lastFrameTime = glutGet(GLUT_ELAPSED_TIME);
    }

    int now = glutGet(GLUT_ELAPSED_TIME);
    int elapsedMilliseconds = now - lastFrameTime;
    float elapsedTime = elapsedMilliseconds / 1000.0f;
    lastFrameTime = now;

    int windowWidth = glutGet(GLUT_WINDOW_WIDTH);
    int windowHeight = glutGet(GLUT_WINDOW_HEIGHT);

    // draw stationary boxes
    for (x=0;x<N;x++)
    {
    	if (x!=currentBox)
    		drawBox(0,yAxis[x]);
    }

    int x_offset = speed * elapsedTime;

    switch (animateFlag)
    {
    case 1:
    	boxX += x_offset;
    	boxY += x_offset * tan(ranAngle[currentBox]*M_PI/180);
    	break;
    case 2:
    	boxX -= x_offset;
    	boxY -= x_offset * tan(ranAngle[currentBox]*M_PI/180);
    	break;
    }

    if (boxX > windowWidth || (boxY + yAxis[currentBox]) > windowHeight || (boxY + yAxis[currentBox]) <= 0 )
    {
        /*boxX = 0;
        boxY = 0;*/
        animateFlag = 2;
    }

    if (animateFlag==2 && boxX<=0)
    {
        boxX = 0;
        boxY = 0;
    	animateFlag = 0;
    }

    // draw flying box
    glPushMatrix();
    glTranslatef(boxX, boxY, 0.0f);
    drawBox(0,yAxis[currentBox]);
    glPopMatrix();

    glutSwapBuffers();
}

void processNormalKeys(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'a':
		speed -= 50;
		break;
	case 'd':
		speed += 50;
		break;
	case 's':
		speed = 200;
		break;
	case ' ':
		animateFlag = 1;
		currentBox++;
		if (yAxis[currentBox]==-1)
			currentBox = 0;
		lastFrameTime = glutGet(GLUT_ELAPSED_TIME);
		break;
	case 27:	// esc
		exit(0);
	}
}

void reshape(int width, int height)
{
    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, width, 0, height);
    glMatrixMode(GL_MODELVIEW);
}

void idle(void)
{
	switch (animateFlag)
	{
	case 0:
		// we won't be redrawing the screen here... its kinda dumb...
		break;
	case 1:
	case 2:
		animation();
		break;
	}

}

void randomYStart()
{
	int x;	//counter
	int windowHeight = glutGet(GLUT_WINDOW_HEIGHT);

	//N = rand() % MAX_N;	// lousy rand... keep choosing 3
	N = 10;

	for (x=0;x<N;x++)
	{
		// this part can use 2d array...
		yAxis[x] = rand() % windowHeight;
	    ranAngle[x] = (rand() % 3100)/100.0f - 15;
	}

	yAxis[x] = -1;	// mark the end of the array;
}

/*added some new comments */
int main(int argc, char** argv)
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
    glutInitWindowSize(1500, 500);

    glutCreateWindow("My First GLUT Program");

	randomYStart();

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(processNormalKeys);
    glutIdleFunc(idle);

    glutMainLoop();
    return 0;
}
